package dev.rehm.data;

import dev.rehm.models.MarketItem;
import dev.rehm.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MarketItemDaoImpl implements  MarketItemDao {

    private final Logger logger = LoggerFactory.getLogger(MarketItemDaoImpl.class);

    /*  existsUser() -> user
        "select count(id) from users where username = ? and password = ?"
     */

    @Override
    public List<MarketItem> getAllItems() {
        List<MarketItem> items = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection();
             Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery("select * from market_item");

            while(resultSet.next()){
                int id = resultSet.getInt(MarketItemMapping.ID);
                double price = resultSet.getDouble(MarketItemMapping.PRICE);
                String name = resultSet.getString(MarketItemMapping.NAME);
                MarketItem item = new MarketItem(id,price,name);
                items.add(item);
            }
            logger.info("selecting all items from db - {} items retrieved", items.size());
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public MarketItem getItemById(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from market_item where id = ?")) {
            prepareStatement.setInt(1,id);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                double price = resultSet.getDouble(MarketItemMapping.PRICE);
                String name = resultSet.getString(MarketItemMapping.NAME);
                logger.info("1 item retrieved from database by id: {}", id);
                return new MarketItem(id, price, name);
            }
        } catch (SQLException e) {
            logException(e);
        }
        return null;
    }

    @Override
    public List<MarketItem> getItemsInRange(double min, double max) {
        List<MarketItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from market_item where price > ? and price < ?")) {
            prepareStatement.setDouble(1,min);
            prepareStatement.setDouble(2,max);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(MarketItemMapping.ID);
                double price = resultSet.getDouble(MarketItemMapping.PRICE);
                String name = resultSet.getString(MarketItemMapping.NAME);
                items.add(new MarketItem(id, price, name));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public List<MarketItem> getItemsWithMaxPrice(double max) {
        List<MarketItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from market_item where price < ?")) {
            prepareStatement.setDouble(1,max);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(MarketItemMapping.ID);
                double price = resultSet.getDouble(MarketItemMapping.PRICE);
                String name = resultSet.getString(MarketItemMapping.NAME);
                items.add(new MarketItem(id, price, name));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public List<MarketItem> getItemsWithMinPrice(double min) {
        List<MarketItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement("select * from market_item where price > ?")) {
            prepareStatement.setDouble(1,min);
            ResultSet resultSet = prepareStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(MarketItemMapping.ID);
                double price = resultSet.getDouble(MarketItemMapping.PRICE);
                String name = resultSet.getString(MarketItemMapping.NAME);
                items.add(new MarketItem(id, price, name));
            }
        } catch (SQLException e) {
            logException(e);
        }
        return items;
    }

    @Override
    public MarketItem addNewItem(MarketItem item) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareCall("select * from create_market_item( ?, ? )")){
            preparedStatement.setString(1, item.getName());
            preparedStatement.setDouble(2, item.getPrice());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                int newId = resultSet.getInt("id");
                item.setId(newId);
                System.out.println(item);
                logger.info("successfully added new item with id: {}",newId);
            } else {
                logger.error("there was an issue adding new item to the db");
                return null;
            }

        } catch (SQLException e) {
            logException(e);
        }
        return item;
    }

    @Override
    public void deleteItem(int id) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from market_item where id = ?")){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.info("deleted item with id: {}", id);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }

    private static class MarketItemMapping {
        private static final String ID = "id";
        private static final String NAME = "name";
        private static final String PRICE = "price";
    }
}

