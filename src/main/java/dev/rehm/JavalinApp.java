package dev.rehm;

import dev.rehm.controllers.AuthController;
import dev.rehm.controllers.ItemController;
import dev.rehm.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;

public class JavalinApp {

    ItemController itemController = new ItemController();
    AuthController authController = new AuthController();
    SecurityUtil securityUtil = new SecurityUtil();

    // adding
    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{
        path("items", ()->{
            before("/",authController::authorizeToken);
            get(itemController::handleGetItemsRequest);
            post(itemController::handlePostNewItem);
            path(":id",()->{
                before("/", authController::authorizeToken);
                get(itemController::handleGetItemByIdRequest);
                delete(itemController::handleDeleteById);
            });
        });
        path("login",()-> {
            post(authController::authenticateLogin);
            after("/", securityUtil::attachResponseHeaders);
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }

}
