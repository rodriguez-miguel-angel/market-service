package dev.rehm.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    private ConnectionUtil(){
        super();
    }

    public static Connection getConnection() throws SQLException {
        if(connection == null || connection.isClosed()) {
            String connectionUrl = System.getenv("connectionUrl");
            String username = System.getenv("username");
            String password = System.getenv("password");

            connection = DriverManager.getConnection(connectionUrl, username, password);
        }
        return connection;
    }

}
