package dev.rehm.controllers;

import dev.rehm.models.MarketItem;
import dev.rehm.services.MarketItemService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemController {

    private final Logger logger = LoggerFactory.getLogger(ItemController.class);
    private MarketItemService service = new MarketItemService();


    public void handleGetItemsRequest(Context ctx){
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
           logger.info("getting items in price range");
           ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            logger.info("getting all items");
            ctx.json(service.getAll());
        }
    }

    public void handleGetItemByIdRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            MarketItem item = service.getById(idInput);
            if (item == null) {
                logger.warn("no item present with id: {}", idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: {}", idInput);
                ctx.json(item);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewItem(Context ctx){
        MarketItem item = ctx.bodyAsClass(MarketItem.class);
        logger.info("adding new item: {}", item);
        MarketItem newItem = service.add(item);
        ctx.status(201);
        ctx.json(newItem);
    }

    public void handleDeleteById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: {}", idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

}
