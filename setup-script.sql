create table market_item(
	id serial primary key,
	name varchar(50),
	price numeric(6,2)
)


insert into market_item  (name, price) values ('unicycle',75.00);
insert into market_item (name, price) values ('helmet', 30);
insert into market_item (name, price) values ('energy bars', 3);
insert into market_item (name, price) values ('kayak', 1500);
insert into market_item (name, price) values ('paddle', 100);